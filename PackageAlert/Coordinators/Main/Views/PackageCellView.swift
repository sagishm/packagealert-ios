//
//  PackageCellView.swift
//  PackageAlert
//
//  Created by Sagi on 12/28/18.
//  Copyright © 2018 Sagi. All rights reserved.
//

import UIKit

class PackageCellView: UITableViewCell {
    let label: UILabel = {
       let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubview(label)
        label.snp.makeConstraints { make in
            make.margins.equalToSuperview()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension PackageCellView: CellConfigurable {
    func setup(viewModel: PackageCellViewModel) {
        self.label.text = viewModel.text
    }
}
