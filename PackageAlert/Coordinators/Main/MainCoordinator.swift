//
//  MainCoordinator.swift
//  PackageAlert
//
//  Created by Sagi on 12/24/18.
//  Copyright © 2018 Sagi. All rights reserved.
//

import UIKit

class MainCoordinator: Coordinator {
    var hostView: UIWindow
    
    init(hostView: UIWindow,context: AppContext) {
        self.hostView = hostView
    }
    
    func start() {
        let viewModel = MainViewModel()
        let vc = MainViewController(viewModel: viewModel).embdedInNavigationController().withAppStyle()
        
        hostView.rootViewController = vc
    }
    
    
}
