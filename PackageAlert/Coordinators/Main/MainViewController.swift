//
//  MainViewController.swift
//  PackageAlert
//
//  Created by Sagi on 12/24/18.
//  Copyright © 2018 Sagi. All rights reserved.
//

import UIKit
import SnapKit

class MainViewController: UIViewController {
    var viewModel: MainViewModel
    
    let tableView: UITableView
    
    public init(viewModel: MainViewModel) {
        self.viewModel = viewModel
        self.tableView = UITableView(frame: CGRect.zero)
        self.tableView.backgroundColor = .white
        super.init(nibName: nil, bundle: nil)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "test"

        view.addSubview(tableView)

        tableView.register(PackageCellView.self)
        
        tableView.delegate = self
        tableView.dataSource = self
        
        buildConstraints()
    }
    
    func buildConstraints() {
        tableView.snp.makeConstraints { make in
            make.margins.equalToSuperview()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension MainViewController: UITableViewDelegate {
    
}

extension MainViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let viewModel: PackageCellViewModel = PackageCellViewModel()
        let cell: PackageCellView = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.setup(viewModel: viewModel)
        return cell
    }
    
    
}
