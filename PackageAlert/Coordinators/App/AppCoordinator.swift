//
//  AppCoordinator.swift
//  PackageAlert
//
//  Created by Sagi on 12/24/18.
//  Copyright © 2018 Sagi. All rights reserved.
//

import UIKit

class AppCoordinator: Coordinator {
    public var hostView: UIWindow
    
    var context: AppContext {
        return AppContext()
    }
    
    public init (hostView: UIWindow) {
        self.hostView = hostView
    }
    
    func start() {
        let coordinator = MainCoordinator(hostView: hostView, context: context)
        coordinator.start()
    }
    
    
}
