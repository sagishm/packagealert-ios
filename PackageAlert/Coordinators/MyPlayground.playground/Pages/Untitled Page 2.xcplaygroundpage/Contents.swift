//: [Previous](@previous)

import UIKit

func isPalindrome(myString:String) -> Bool {
    var string = myString.replacingOccurrences(of: "\\W", with: "", options: .regularExpression, range: nil).lowercased()

    var reverseString = String(string.characters.reversed()).lowercased()
    
    if(string == reverseString) {
        return true
    } else {
        return false
    }
}

func myAtoi(_ str: String) -> Int {
    let i = str.components(separatedBy: CharacterSet.decimalDigits.union(CharacterSet(charactersIn: "-"))).joined(separator: "")
    return Int(i) ?? 0
}

func strStr(_ haystack: String, _ needle: String) -> Int {
    if needle.characters.count == 0 {
        return 0
    }else if let range = haystack.range(of: needle) {
        return haystack.distance(from: haystack.startIndex, to: range.lowerBound)
    }
    return -1
}

strStr("sagi shmuek", "sh")
//isPalindrome(myString: "A man, a plan, a canal: Panama")
//: [Next](@next
