import UIKit

func validateCode(code: String) -> Bool {
    let chars: [Character] = ["{","}","(",")"]
    var stack: [Character] = []

    for char in code {
        if let checkOpenIndex = chars.index(of: char) {
            if checkOpenIndex % 2 == 0 { // open
                stack.append(char)
            } else { // close
                if stack.isEmpty {
                    return false
                }
                if let checkCloseIndex = chars.index(of: char),
                    let stackLast = stack.last ,
                    let checkOpenIndexStack = chars.index(of: stackLast) {
                    if checkCloseIndex - checkOpenIndexStack == 1 {
                        stack.removeLast()
                    } else {
                        return false
                    }
                }
                
            }
        }
        
 
    }

    print(stack)
    return stack.isEmpty
}


validateCode(code: "{()s}")
