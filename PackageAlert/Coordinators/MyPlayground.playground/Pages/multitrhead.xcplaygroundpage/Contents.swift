//: [Previous](@previous)

import UIKit
import PlaygroundSupport

PlaygroundPage.current.needsIndefiniteExecution = true

class Singlton {
    static let shared = Singlton()
    
    init(){}
    
    var text: String?
}
var str = "Hello, playground"

DispatchQueue.global(qos: .background).sync {
    str = "123"
}
let serial = DispatchQueue(label: "Queuename")

serial.sync {
    str = "111"
}
print(str)
//: [Next](@next)
