//: [Previous](@previous)

import UIKit

func twoSum(_ nums: [Int], _ target: Int) -> [Int] {
    var stack: [Int: Int] = [:]
    var res: [Int] = []
    for (i, num) in nums.enumerated() {
        if let value = stack[num] {
            res = [value, i]
        } 
        stack[target - num] = i
    }
    
    return res
}

func reverse(_ x: Int) -> Int {
    var string = String(x)
    var saveFirstChar: Character?
    if let s = string.first {
        let char = String(s)
        let isNum = Int(char)
        if isNum == nil {
            saveFirstChar = Character(char)
            string.removeFirst()
        }
    }
    var reverse = String(string.reversed())
    if let saveFirstChar = saveFirstChar {
        reverse.insert(saveFirstChar, at: string.startIndex)
    }
    
    if let reverseToInt = Int32(reverse) {
        return Int(reverseToInt)
    }
    return 0
    
}

func firstUniqChar(_ s: String) -> Int {
    for (i,char) in s.enumerated() {
        var x = 0
        if s.contains(char) {
            return i
        }
    }
    return -1
}
func isAnagram(_ s: String, _ t: String) -> Bool {
    return s.lowercased().sorted() == t.lowercased().sorted()
    
}

func isPalindrome(_ s: String) -> Bool {
    let newString = String(s.reversed())
    if newString == s {
        return true
    } else {
        return false
    }
    
}

isPalindrome("Panama")
//isAnagram("anagram", "nagaram")
//firstUniqChar("sagas")
//firstUniqChar("")
//reverse(1534236469)
//twoSum([1, 5, 9, 3], 8)
//
//reverse(-123)
//: [Next](@next)
