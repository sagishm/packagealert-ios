//: [Previous](@previous)

import Foundation

//func removeDuplicates(_ nums: [Int]) -> Int {
//    var lastNum: Int?
//    var count = 0
//
//    for (i, num) in nums.enumerated() {
//        if let last = lastNum {
//            if last == num {
//                count += 1
//            }
//        }
//        lastNum = num
//    }
//    return count
//}

func rotate(_ nums: [Int], _ k: Int) {
    var newArray = nums
    
    for _ in 1...k {
        if let lastNum = newArray.last {
            newArray.removeLast()
            newArray.insert(lastNum, at: 0)
        }
    }
    print(newArray)
}

func containsDuplicate(_ nums: [Int]) -> Bool {
    return Set(nums).count != nums.count
}

func singleNumber(_ nums: [Int]) -> Int {
    var result = 0
    for num in nums {
        result ^= num
    }
    return result
}

func plusOne1(_ digits: [Int]) -> [Int] {
    var digits = digits
    var index = digits.count - 1
    
    while index >= 0 {
        if digits[index] < 9 {
            digits[index] += 1
            return digits
        }
        
        digits[index] = 0
        index -= 1
        print(index)
    }
    
    digits.insert(1, at: 0)
    return digits
}

func moveZeroes(_ nums: [Int]) {
    var numsArray = nums.filter { $0 != 0 }
    let zeros = nums.filter { $0 == 0 }
    
    numsArray.append(contentsOf: zeros)
    numsArray
}

func rotate(_ matrix: [[Int]]) {
    var rotateStack: [[Int]] = []
    var array: [Int] = []
    var aCount = 0

    for (x,a) in matrix.enumerated() {
        aCount = a.count
        for y in 0..<a.count {
            array.append(a[y])
        }
    }
    
    
    for (x,ar) in matrix.enumerated() {
        var newArray: [Int] = []
        for (i, n) in array.enumerated() {
            
            if i % aCount == x {
                newArray.insert(n, at: 0)
            }
        }
        rotateStack.append(newArray)
    }


    print(rotateStack)
}

func reverseString(_ string: String) -> String {
    var newString: String = ""
    for s in string {
        newString.insert(s, at: string.startIndex)
    }
    return newString
}

//reverseString("sagi shmuel")

rotate([[1,2,3,4],
        [4,5,6,7],
        [7,8,9,10],
        [10,11,12,13]])

//moveZeroes([1,0,2,0,3,9])

//rotate([1,2,3,4,5,6], 3)
//removeDuplicates([1,1,2,2,3,3])
//: [Next](@next)
//singleNumber([4,4,2,1,2])
