//
//  ViewController.swift
//  PackageAlert
//
//  Created by Sagi on 12/7/18.
//  Copyright © 2018 Sagi. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var label: UILabel!
    
    @IBAction func didTap(_ sender: Any) {
        label.text = "asd asdsa das\nasdasd asdasdas\nasd asdsa das\nasdasd asdasdasd"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let webservice = Webservice()
        let body = TestBody(text: "heb")
        let resource = Resource<TestResult, TestBody>(path: "/getLimits").withPost(body)
        webservice.load(resource: resource) { (result) in
            result.withValue({ (response) in
                
            })
        }
        // Do any additional setup after loading the view, typically from a nib.
    }


}

struct TestBody: Codable {
    var text: String
//    var id: String
    
    init(text: String) {
        self.text = text
//        self.id = id
    }
}

struct TestResult: Codable {
    var code: Int
}
