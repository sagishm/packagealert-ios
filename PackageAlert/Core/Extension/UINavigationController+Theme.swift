//
//  UINavigationController+Theme.swift
//  PackageAlert
//
//  Created by Sagi on 12/24/18.
//  Copyright © 2018 Sagi. All rights reserved.
//

import UIKit

extension UINavigationController {
    
    func withTranlucentAppStyle() -> UINavigationController {
        let navBar = self.navigationBar
        
        navBar.isTranslucent = true
        navBar.tintColor = Style.color.navBarTintColor
        navBar.barTintColor = Style.color.background
        navBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Style.color.navBarTitleColor]
        
        // Defaults
        navBar.shadowImage = nil
        navBar.setBackgroundImage(nil, for: UIBarMetrics.default)
        return self
    }
    
    /// Theme the navigation bar with transparent style
    /// In addition, set the tintColor for controls and title's text attributes.
    /// - Returns: Self
    @discardableResult func withTransparentStyle() -> UINavigationController {
        let navBar = self.navigationBar
        navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navBar.shadowImage = UIImage()
        navBar.isTranslucent = true
        navBar.tintColor = Style.color.navBarTintColor
        navBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Style.color.navBarTitleColor,
                                      NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14)]
        
        return self
    }
    
    /// Theme the navigation bar with solid color, the app's primary color.
    /// In addition, set the tintColor for controls and title's text attributes.
    /// - Returns: self
    @discardableResult func withAppStyle() -> UINavigationController {
        let navigationBar = self.navigationBar
        
        // to have a single solid color in the home header and the nav bar we can't use translucent nav bar.
        // so it's turned off.
        navigationBar.isTranslucent = false
        navigationBar.tintColor = Style.color.navBarTintColor
        navigationBar.barTintColor = Style.color.barTintColor
        navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Style.color.navBarTitleColor,
                                             NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14)]
        
        navigationBar.shadowImage = UIImage()
        
        if #available(iOS 11.0, *) {
            // in iOS 11 there is no longer need to set the background image to hide the shadow line.
        } else {
            // on older iOS versions we do need to set the background image to hide the nav bar shadow
//            let backgroundImage = UIImage.fromColor(Style.color.background)
//                .resizableImage(withCapInsets: .zero, resizingMode: .stretch)
//            navigationBar.setBackgroundImage(backgroundImage, for: UIBarMetrics.default)
        }
        
        return self
    }
}
