//
//  UIViewController.swift
//  PackageAlert
//
//  Created by Sagi on 12/24/18.
//  Copyright © 2018 Sagi. All rights reserved.
//

import UIKit

extension UIViewController {
    func embdedInNavigationController() -> UINavigationController {
        let nvc = UINavigationController(rootViewController: self)
        return nvc
    }
}
