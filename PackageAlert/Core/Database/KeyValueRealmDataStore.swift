//
//  KeyValueDataStore.swift
//  PackageAlert
//
//  Created by Sagi on 12/13/18.
//  Copyright © 2018 Sagi. All rights reserved.
//

import Foundation
import RealmSwift

final class RLMKeyValueRow: Object {
    @objc dynamic var key: String = ""
    @objc dynamic var data: Data = Data()
    
    override static func primaryKey() -> String? {
        return "key"
    }
    
    convenience init(key: String, data: Data) {
        self.init()
        self.key = key
        self.data = data
    }
}

final class KeyValueRealmDataStore {
    
    let provider = CreateRealmProvider()
    init() {}
    
    func get(forKey key: String) -> Data? {
        do {
            let realm = try provider.getRealm()
            let result = realm.object(ofType: RLMKeyValueRow.self, forPrimaryKey: key)
            return result?.data
        } catch {
            RealmHelper.handleError(error)
        }
        return nil
    }
    
    func set(data: Data, forKey key: String) {
        let obj = RLMKeyValueRow(key: key, data: data)
        
        do {
            let realm = try provider.getRealm()
            try realm.write {
                realm.add(obj, update: true)
            }
        } catch {
            RealmHelper.handleError(error)
        }
    }
    
    func remove(forKey key: String) {
        do {
            let realm = try provider.getRealm()
            try realm.write {
                if let obj = realm.object(ofType: RLMKeyValueRow.self, forPrimaryKey: key) {
                    realm.delete(obj)
                }
            }
        } catch {
            RealmHelper.handleError(error)
        }
    }
    
    func removeAll() {
        do {
            let realm = try provider.getRealm()
            try realm.write {
                let results = realm.objects(RLMKeyValueRow.self)
                realm.delete(results)
            }
        } catch {
            RealmHelper.handleError(error)
        }
    }
}
