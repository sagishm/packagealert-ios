//
//  RealmHelper.swift
//  PackageAlert
//
//  Created by Sagi on 12/13/18.
//  Copyright © 2018 Sagi. All rights reserved.
//

import Foundation
import RealmSwift

final class RealmHelper {
    
    /// Update this version whenever you change the structure of a JSON stored in realm
    /// Currently this will force user to logout.
    /// Ideally we would have migration that will just clear the sync data but keep the user credentials.
    static let schemaVersion: UInt64 = 9
    
    static let shared = RealmHelper()
    
    var defaultConfiguration: Realm.Configuration
    
    var keepAliveRealm: Realm?
    
    init() {
        var config = Realm.Configuration(schemaVersion: RealmHelper.schemaVersion, migrationBlock: RealmHelper.migrationHandler)
        
        //config.deleteRealmIfMigrationNeeded = true
        
        // For memory consumpstion we can declare only specific models we use
        // https://realm.io/docs/swift/latest/#operating-with-low-memory-constraints
        config.objectTypes = [RLMKeyValueRow.self]
        
        self.defaultConfiguration = config
        
        Realm.Configuration.defaultConfiguration = defaultConfiguration
        
    }
    
    func setup() {
        assert(Thread.isMainThread)
        
        // keeping at least one Realm of our database will keep the cache alive.
        if keepAliveRealm == nil {
            do {
                keepAliveRealm = try Realm(configuration: self.defaultConfiguration)
            } catch {
                print("[realm helper] failed to setup a realm. error: \(error)")
                RealmHelper.shared.resetRealmDb()
                keepAliveRealm = try? Realm(configuration: self.defaultConfiguration)
            }
        }
    }
    
    func newRealm(retyIfFailed: Bool = false) throws -> Realm {
        do {
            return try Realm(configuration: self.defaultConfiguration)
        } catch let error as NSError {
            print("Failed to open Realm. error:\(error.domain), \(error.code), \(error.localizedDescription)")
            if retyIfFailed {
                RealmHelper.shared.resetRealmDb()
            }
        }
        
        return try RealmHelper.shared.newRealm()
    }
    
    func deleteAll() {
        guard let realm = try? newRealm(retyIfFailed: false) else {
            resetRealmDb()
            return
        }
        
        do {
            try realm.write {
                realm.deleteAll()
            }
        } catch {
            RealmHelper.shared.resetRealmDb()
        }
    }
    
    func resetRealmDb() {
        print("#########   reset Realm Db #########")
        guard let realmURL = self.defaultConfiguration.fileURL else {
            return
        }
        
        keepAliveRealm = nil
        
        let realmURLs = [
            realmURL,
            realmURL.appendingPathExtension("lock"),
            realmURL.appendingPathExtension("note"),
            realmURL.appendingPathExtension("management")
        ]
        
        for URL in realmURLs {
            do {
                print("[realm-helper] delete \(URL.absoluteString)")
                try FileManager.default.removeItem(at: URL)
            } catch let error as NSError {
                // handle error
                print("Failed reset Realm Db. error:\(error.domain), \(error.code), \(error.localizedDescription)")
            }
        }
    }
    
    static func handleError(_ error: Error) {
        let nsError = error as NSError
        print("[realm-helper] write failed error:\(nsError.domain), \(nsError.code), \(nsError.localizedDescription)")
    }
    
    static func handleFatalError(_ error: Error) {
        let nsError = error as NSError
        print("[realm-helper] write failed error:\(nsError.domain), \(nsError.code), \(nsError.localizedDescription)")
        RealmHelper.shared.resetRealmDb()
    }
    
}

extension RealmHelper {
    private static func migrationHandler(_ migration: Migration, _ oldSchemaVersion: UInt64) {
        debugPrint("Start migration. oldSchemaVersion: \(oldSchemaVersion) schemaVersion: \(RealmHelper.schemaVersion)")
        
        if oldSchemaVersion != RealmHelper.schemaVersion {
            migration.enumerateObjects(ofType: RLMKeyValueRow.className()) { oldObject, newObject in
                // combine name fields into a single field
                
                if let key = oldObject?["key"] as? String {
                    if let newObject = newObject {
                        print("migration: clear ")
                        migration.delete(newObject)
                    }
                }
            }
        }
        
    }
}
