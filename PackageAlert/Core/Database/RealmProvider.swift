//
//  RealmProvidr.swift
//  PackageAlert
//
//  Created by Sagi on 12/13/18.
//  Copyright © 2018 Sagi. All rights reserved.
//

import Foundation
import RealmSwift

protocol RealmProvider {
    func getRealm() throws -> Realm
}

final class CreateRealmProvider: RealmProvider {
    
    func getRealm() throws -> Realm {
        return try RealmHelper.shared.newRealm(retyIfFailed: true)
    }
}

final class ResuableRealmProvider: RealmProvider {
    private var _realm: Realm!
    
    func getRealm() throws -> Realm {
        if _realm == nil {
            _realm = try CreateRealmProvider().getRealm()
        }
        return _realm
    }
}
