//
//  Style.swift
//  PackageAlert
//
//  Created by Sagi on 12/24/18.
//  Copyright © 2018 Sagi. All rights reserved.
//

import UIKit

struct Style {
    enum color {
        static let navBarTintColor = UIColor.lightGray
        static let background = UIColor.white
        static let barTintColor = UIColor(hex: 0xF5B041)
        
        static let navBarTitleColor = UIColor.darkGray
    }
}
