//
//  Context.swift
//  PackageAlert
//
//  Created by Sagi on 12/24/18.
//  Copyright © 2018 Sagi. All rights reserved.
//

import Foundation

protocol WebServiceProviding {
    var webservice: Webservice { get }
}

class AppContext: WebServiceProviding {
    var webservice: Webservice {
        return Webservice()
    }
}


