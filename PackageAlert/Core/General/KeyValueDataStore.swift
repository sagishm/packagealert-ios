//
//  KeyValueDataStore.swift
//  PackageAlert
//
//  Created by Sagi on 12/23/18.
//  Copyright © 2018 Sagi. All rights reserved.
//

import Foundation

final class KeyValueDataStore {
    
    struct Box: Codable {
        var d: Date?
        var s: String?
        var i: Int?
        var f: Float?
        var g: Double?
        var b: Bool?
        
        init(_ value: Date?) {
            self.d = value
        }
        
        init(_ value: String?) {
            self.s = value
        }
        
        init(_ value: Int?) {
            self.i = value
        }
        
        init(_ value: Float?) {
            self.f = value
        }
        
        init(_ value: Double?) {
            self.g = value
        }
        
        init(_ value: Bool?) {
            self.b = value
        }
    }
    
    var store = KeyValueRealmDataStore()
    
    lazy var encoder: JSONEncoder = {
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .formatted(AppDateFormatter.iso8601DateFormatter())
        return encoder
    }()
    
    lazy var decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(AppDateFormatter.iso8601DateFormatter())
        return decoder
    }()
    
    let codingQueue = DispatchQueue(label: "kvdatastore", qos: DispatchQoS.utility)
    
    init() { }
    
    func get<T>(_ type: T.Type, forKey key: String) -> T? where T: Decodable {
        
        let data = store.get(forKey: key)
        
        guard let unwrappedData = data else {
            return nil
        }
        
        return codingQueue.sync {
            //            let decoder = JSONDecoder()
            //            decoder.dateDecodingStrategy = .formatted(PayboxDateFormatter.iso8601DateFormatter())
            
            return try? decoder.decode(type, from: unwrappedData)
        }
    }
    
    func set<T>(value: T?, forKey key: String) where T: Codable {
        
        guard let encodable = value else {
            store.remove(forKey: key)
            return
        }
        
        let data: Data? = codingQueue.sync {
            let result: Data?
            do {
                result = try encoder.encode(encodable)
                return result
            } catch {
                #if DEBUG
                print("[encoder] encoder failed. \(error)")
                #endif
            }
            return nil
        }
        
        if let data = data {
            store.set(data: data, forKey: key)
        } else {
            print("[KeyValueDataStore] encoding failed")
        }
    }
    
    func string(forKey key: String) -> String? {
        return get(Box.self, forKey: key)?.s
    }
    
    func stringArray(forKey key: String) -> [String]? {
        return get([String].self, forKey: key)
    }
    
    func integer(forKey key: String) -> Int? {
        return get(Box.self, forKey: key)?.i
    }
    
    func float(forKey key: String) -> Float? {
        return get(Box.self, forKey: key)?.f
    }
    
    func double(forKey key: String) -> Double? {
        return get(Box.self, forKey: key)?.g
    }
    
    func bool(forKey key: String) -> Bool? {
        return get(Box.self, forKey: key)?.b
    }
    
    func date(forKey key: String) -> Date? {
        return get(Box.self, forKey: key)?.d
    }
    
    // MARK: Setters
    
    func set(value: Int?, forKey key: String) {
        set(value: Box(value), forKey: key)
    }
    
    func set(value: Float?, forKey key: String) {
        set(value: Box(value), forKey: key)
    }
    
    func set(value: Double?, forKey key: String) {
        set(value: Box(value), forKey: key)
    }
    
    func set(value: Bool?, forKey key: String) {
        set(value: Box(value), forKey: key)
    }
    
    func set(value: Date?, forKey key: String) {
        set(value: Box(value), forKey: key)
    }
    
    func set(value: String?, forKey key: String) {
        set(value: Box(value), forKey: key)
    }
    
    func remove(forKey key: String) {
        store.remove(forKey: key)
    }
}
