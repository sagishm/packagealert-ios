//
//  Prefs.swift
//  PackageAlert
//
//  Created by Sagi on 12/13/18.
//  Copyright © 2018 Sagi. All rights reserved.
//

import Foundation

enum Prefs {
    final class User {
        static let shared = User()
        private init() { }
        private var store: KeyValueDataStore { return KeyValueDataStore() }
        
        private enum Keys: String {
            case userData
            
            static let allKeys: [Keys] = [.userData]
        }
        
        func reset() {
            for key in Keys.allKeys {
                store.remove(forKey: key.rawValue)
            }
        }
        
        var userData: UserData? {
            set { store.set(value: newValue, forKey: Keys.userData.rawValue)  }
            get { return store.get(UserData.self, forKey: Keys.userData.rawValue) }
        }
    }
}
