//
//  AppSession.swift
//  PackageAlert
//
//  Created by Sagi on 12/13/18.
//  Copyright © 2018 Sagi. All rights reserved.
//

import Foundation

struct AppSession {
    
    static var current: Session!
    
    private init() { }
    
    static func setup() {
        AppSession.replaceSession(AppSession.lastStoredSession())
    }
    
    static func lastStoredSession() -> Session {
        return Session(userData: Prefs.User.shared.userData)
    }
    
    static func resetSession() {
        AppSession.replaceSession(userData: nil)
    }
    
    static func login(with userData: UserData) {
        update(userData: userData)
    }
    
    static func update(userData: UserData?) {
        AppSession.replaceSession(userData: userData)
    }
    
    static func replaceSession(_ session: Session) {
        current = session
    }
    
    static func replaceSession(userData: UserData? = AppSession.current.userData) {
        let newSession = Session(userData: userData)
        current = newSession
    }
}
