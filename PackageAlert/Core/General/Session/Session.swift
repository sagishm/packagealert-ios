//
//  Session.swift
//  PackageAlert
//
//  Created by Sagi on 12/13/18.
//  Copyright © 2018 Sagi. All rights reserved.
//

import Foundation

struct Session {
    var userData: UserData?
    
    var isAuthenticated: Bool {
        return userData != nil
    }
    
    init(userData: UserData?) {
        self.userData = userData
    }
}
