//
//  CellConfigurable.swift
//  PackageAlert
//
//  Created by Sagi on 12/28/18.
//  Copyright © 2018 Sagi. All rights reserved.
//

import Foundation

protocol CellViewModel { }

protocol CellConfigurable {
    associatedtype ItemType: CellViewModel

    func setup(viewModel: ItemType)
}
