//
//  Coordinator.swift
//  PackageAlert
//
//  Created by Sagi on 12/14/18.
//  Copyright © 2018 Sagi. All rights reserved.
//

import Foundation

protocol Coordinator {
    func start()
}
