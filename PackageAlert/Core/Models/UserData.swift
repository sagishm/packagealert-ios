//
//  UserData.swift
//  PackageAlert
//
//  Created by Sagi on 12/13/18.
//  Copyright © 2018 Sagi. All rights reserved.
//

import Foundation

struct UserData: Codable {
    var uid: String
    var lastOpen: Date
}
