//
//  DateFormatter.swift
//  PackageAlert
//
//  Created by Sagi on 12/7/18.
//  Copyright © 2018 Sagi. All rights reserved.
//

import Foundation

public let UTCTimeZone = TimeZone(secondsFromGMT: 0)!

class AppDateFormatter {
    static func iso8601DateFormatter() -> DateFormatter {
        let iso8601Formatter = DateFormatter()
        iso8601Formatter.locale = Locale(identifier: "en_US_POSIX")
        iso8601Formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'" // 2015-10-19T00:00:00.123Z
        iso8601Formatter.timeZone = UTCTimeZone
        return iso8601Formatter
    }

}
