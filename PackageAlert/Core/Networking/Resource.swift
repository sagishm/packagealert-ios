//
//  Resource.swift
//  PackageAlert
//
//  Created by Sagi on 12/7/18.
//  Copyright © 2018 Sagi. All rights reserved.
//

import Foundation

enum ResourceURLError: Swift.Error {
    case unknown
    case cannotComposeURL
    case decodingFailed(Error)
    case serverError(Int)
}

enum HttpMethod<Body: Encodable> {
    case get
    case post(data: Body?)
    case put(data: Body?)
    
    var method: String {
        switch self {
        case .get: return "GET"
        case .post: return "POST"
        case .put: return "PUT"
        }
    }
}

struct Resource<Response: Codable, Body: Encodable> {
    var baseUrl: URL?
    var path: String
    var queryItems: [URLQueryItem] = []
    var method: HttpMethod<Body> = .get
    var headers: [String: String] = ["Content-Type": "application/json; charset=utf-8",
                                     "Accept": "application/json; charset=utf-8"]
    var useJSONHeaders = true
    
    // calculated by webservice when it calls getURL()
    var url: URL?
    
    var timeoutInterval: Int?
    
    init(path: String, method: HttpMethod<Body> = .get) {
        self.method = method
        self.path = path
        self.baseUrl = URL(string: AppConfig.baseURL)
    }
    
    init(path: String,
         query: [URLQueryItem] = [],
         headers: [String: String] = [:],
         method: HttpMethod<Body> = .get) {
        self.path = path
        self.queryItems = query
        self.method = method
    }
    
    func with(useJSONHeaders: Bool) -> Resource<Response, Body> {
        var resource = self
        resource.useJSONHeaders = useJSONHeaders
        return resource
    }
    
    func with(method: HttpMethod<Body>) -> Resource<Response, Body> {
        var resource = self
        resource.method = method
        return resource
    }
    
    func withPost(_ body: Body?) -> Resource<Response, Body> {
        var resource = self
        resource.method = HttpMethod<Body>.post(data: body)
        return resource
    }
    
    func with(queryValue value: String?, name: String) -> Resource<Response, Body> {
        let item = URLQueryItem(name: name, value: value)
        var resource = self
        resource.queryItems = [item]
        return resource
    }
    
    func with(queryItems: [URLQueryItem]) -> Resource<Response, Body> {
        var resource = self
        resource.queryItems = queryItems
        return resource
    }
    
    internal func getURL() throws -> URL {
        guard let baseUrl = baseUrl else {
            throw ResourceURLError.cannotComposeURL
        }
        var urlComponents = URLComponents(url: baseUrl.appendingPathComponent(path), resolvingAgainstBaseURL: false)
        if !queryItems.isEmpty {
            urlComponents?.queryItems = queryItems
        }
        guard let url = urlComponents?.url else {
            throw ResourceURLError.cannotComposeURL
        }
        return url
    }
    
    func parse(data: Data) throws -> Response {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(AppDateFormatter.iso8601DateFormatter())
        
        let response: Response
        do {
            response = try decoder.decode(Response.self, from: data)
        } catch {
            if case DecodingError.keyNotFound(let key, let context) = error {
                debugPrint("[Resource] decoding failed. key not found: \(key), context: \(context)")
                throw ResourceURLError.decodingFailed(error)
            } else {
                debugPrint("[Resource] decoding failed:\n\(error)")
            }
            
            throw error
        }
        
        return response
    }
    
    func makeResource<TResponse: Codable>() -> Resource<TResponse, Body> {
        var r = Resource<TResponse, Body>(path: path, query: queryItems, headers: headers, method: method)
        r.baseUrl = baseUrl
        return r
    }
    
    func map<TResponse, TBody>(_ transform: (Resource<Response, Body>) -> Resource<TResponse, TBody>) -> Resource<TResponse, TBody> {
        let r: Resource<TResponse, TBody> = transform(self)
        return r
    }
}

