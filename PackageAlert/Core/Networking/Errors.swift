//
//  Errors.swift
//  PackageAlert
//
//  Created by Sagi on 12/7/18.
//  Copyright © 2018 Sagi. All rights reserved.
//

import Foundation

public enum WebServiceError: Error {
    case createURLRequestFailed
    case clientError(Error)
    case invalidResponse
    case couldNotParse(statusCode: Int, data: Data?, underlyingError: Error) // response with unsuccessful http status code
}

extension WebServiceError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .clientError(let error):
            return "client error: \(error)"
        case .couldNotParse:
            return "coulld not parse"
        case .createURLRequestFailed:
            return "coulld not create URLRequest"
        case .invalidResponse:
            return "invalid response"
        }
    }
}
