//
//  URLRequst.Resource.swift
//  PackageAlert
//
//  Created by Sagi on 12/7/18.
//  Copyright © 2018 Sagi. All rights reserved.
//

import Foundation

extension URLRequest {
    init<Response, Body>(resource: Resource<Response, Body>) throws {
        let url = try resource.getURL()
        
        self.init(url: url)
        
        httpMethod = resource.method.method
        
        switch resource.method {
        case .get: break
        case .post(data: let body):
            let encoder = JSONEncoder()
            encoder.dateEncodingStrategy = .formatted(AppDateFormatter.iso8601DateFormatter())
            httpBody = try encoder.encode(body)
        case .put(data: let body):
            let encoder = JSONEncoder()
            encoder.dateEncodingStrategy = .formatted(AppDateFormatter.iso8601DateFormatter())
            httpBody = try encoder.encode(body)
        }
        
        if resource.useJSONHeaders {
            for (key, value) in resource.headers {
                setValue(value, forHTTPHeaderField: key)
            }
        }
        
        if let timeoutInterval = resource.timeoutInterval {
            self.timeoutInterval = Double(timeoutInterval)
        }
    }
}
