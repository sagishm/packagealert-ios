//
//  Webservice.swift
//  PackageAlert
//
//  Created by Sagi on 12/7/18.
//  Copyright © 2018 Sagi. All rights reserved.
//

import Foundation
import Alamofire

class Webservice {
    func load<Content, Body>(resource: Resource<Content, Body>, _ completion: @escaping (Result<Content>) -> Void) {
        do {
            try load(resource, completion: completion)
        } catch {
            completion(Result.failure(error))
        }
    }
    
    func load<Response, Body>(_ resource: Resource<Response, Body>, completion: @escaping (Result<Response>) -> Void) throws {
        let request: URLRequest
        
        do {
            request = try URLRequest(resource: resource)
        } catch {
            let result = Result<Response>.failure(WebServiceError.createURLRequestFailed)
            DispatchQueue.main.async { completion(result) }
            return
        }
        
        let requestURL = request.url?.absoluteString ?? "-"
        let method = resource.method.method
        
        AF.request(request).responseData { (response) in
            
            let result: Result<Response>
            
            
            func dispatchCompletionOnMain(_ reuslt: Result<Response>) {
                DispatchQueue.main.async {
                    completion(result)
                }
            }
            
            switch(response.result) {
            case .success(_):
                guard let data = response.data else {
                    result = Result.failure(WebServiceError.invalidResponse)
                    dispatchCompletionOnMain(result)
                    return
                }
                
                do {
                    let parsed: Response = try resource.parse(data: data)
                    
                    result = Result.success(parsed)
                    
                } catch {
                        let couldNotParseError = WebServiceError.couldNotParse(statusCode: 300, data: data, underlyingError: error)
                        result = Result.failure(couldNotParseError)
                        print("error")
                }
                
                dispatchCompletionOnMain(result)
                
            case .failure(_):
                break
            }
        }

    }
}
